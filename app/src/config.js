export default {
  env: process.env.NODE_ENV,
  mode: process.env.MODE,
  githubApiEndpoint: process.env.GITHUB_API_ENDPOINT,
  graphqlApiEndpoint: process.env.GRAPHQL_API_ENDPOINT
}
