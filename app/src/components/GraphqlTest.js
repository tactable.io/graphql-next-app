import { useQuery } from "@apollo/react-hooks"
import { gql } from "apollo-boost"

export default function GraphqlTest() {
  const { loading, error, data } = useQuery(gql`
    {
      test {
        id
        firstname
        lastname
      }
    }
  `)

  if (loading) return <p>Loading...</p>
  if (error) return <p>Error :(</p>

  return data.test.map(({ id, firstname, lastname }) => (
    <div key={id}>
      <p>
        {id}) {firstname} {lastname}
      </p>
    </div>
  ))
}
