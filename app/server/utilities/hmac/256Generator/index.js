const crypto = require("crypto")
const uuid = require("uuid/v4")

class Hmac256Generator {
  generateHmacWithSecret(secret) {
    const hmac = crypto.createHmac("sha256", secret)
    const salt = this.generateDefaultSalt()
    hmac.update(salt)
    const digest = hmac.digest("hex")

    return digest
  }

  generateDefaultSalt() {
    return uuid()
  }
}

module.exports = Hmac256Generator
