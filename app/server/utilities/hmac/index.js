const Hmac256Generator = require("./256Generator")

class Hmac {
  constructor() {
    this.hmac256Generator = new Hmac256Generator()
  }

  generateHmac256WithSecret(secret) {
    const hmac = this.hmac256Generator.generateHmacWithSecret(secret)

    return hmac
  }
}

module.exports = Hmac
