const jwt = require("jsonwebtoken")

const JwtImplementation = {
  sign(payload, secret, algorithm) {
    const token = jwt.sign(payload, secret, { algorithm: algorithm })

    return token
  },
  verify(token, secret) {
    try {
      jwt.verify(token, secret)
      return true
    } catch (error) {
      return false
    }
  }
}

module.exports = JwtImplementation
