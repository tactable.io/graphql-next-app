const sandbox = require("sinon").createSandbox()
const expect = require("chai").expect
const jwtRoles = require("@jwt/models/roles")
const roles = require("@models/roles")
const jwt = require("@utilities/jwt")
const jwtImplementation = require("@jwt/implementation")

describe("JWT", () => {
  describe("token generator", () => {
    it("should create token with given payload", () => {
      const payload = jwt.createPayloadForRole(roles.USER)
      const token = jwt.createTokenWithPayload(payload)

      const verification = jwtImplementation.verify(token, "secret")

      expect(verification).to.be.true
    })
  })
  describe("role payload generator", () => {
    it("should create payload for user role", () => {
      const payload = jwt.createPayloadForRole(roles.USER)

      expect(payload)
        .to.have.property("exp")
        .to.equal(jwtRoles.USER.EXPIRATION)

      expect(payload).to.have.property("https://hasura.io/jwt/claims")

      expect(payload)
        .to.have.property("https://hasura.io/jwt/claims")
        .to.have.property("x-hasura-allowed-roles")

      expect(payload)
        .to.have.property("https://hasura.io/jwt/claims")
        .to.have.property("x-hasura-default-role")
    })
  })
})
