const jwtFactory = require("./factories/main")
const jwtPayloadFactory = require("./factories/payload")

//Main JWT module interface for token management
const JWT = {
  createTokenWithPayload(payload) {
    const token = jwtFactory.createTokenWithPayload(payload)

    return token
  },
  createPayloadForRole(role) {
    const payload = jwtPayloadFactory.createPayloadForRole(role)

    return payload
  }
}

module.exports = JWT
