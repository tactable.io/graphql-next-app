module.exports = {
  USER: {
    EXPIRATION: Math.floor(Date.now() / 1000) + 60 * 60 //1 hour since epoch
  }
}
