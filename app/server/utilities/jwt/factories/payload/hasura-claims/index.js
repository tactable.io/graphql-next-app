const roles = require("@models/roles")

const HasuraClaimsFactory = {
  createClaimsForRole(role) {
    switch (role) {
      case roles.USER:
        const claims = {
          "https://hasura.io/jwt/claims": {
            "x-hasura-allowed-roles": ["user"],
            "x-hasura-default-role": "user"
          }
        }

        return claims
      default:
    }
  }
}

module.exports = HasuraClaimsFactory
