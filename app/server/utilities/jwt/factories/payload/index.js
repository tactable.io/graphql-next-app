const roles = require("@models/roles")
const jwtRoles = require("@jwt/models/roles")
const HasuraClaimsFactory = require("./hasura-claims")

const JwtPayloadFactory = {
  createPayloadForRole(role) {
    switch (role) {
      case roles.USER:
        const hasuraClaims = HasuraClaimsFactory.createClaimsForRole(roles.USER)
        const userExpirationDate = { exp: jwtRoles.USER.EXPIRATION }
        const payload = Object.assign({}, hasuraClaims, userExpirationDate)

        return payload
      case roles.ADMIN:
        break

      default:
    }
  }
}

module.exports = JwtPayloadFactory
