"use strict"

const express = require("express")
const jwt = require("@utilities/jwt")
const router = express.Router()

router.get("/:role", async (request, response) => {
  try {
    const role = request.params.role
    const payload = jwt.createPayloadForRole(role)
    const token = jwt.createTokenWithPayload(payload)

    response.status(200).json({
      data: token
    })
  } catch (error) {
    response.status(400).json({
      error: "Could not create JWT token at the moment, please try again later"
    })
  }
})

module.exports = router
