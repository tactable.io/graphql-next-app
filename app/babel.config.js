const fs = require("fs")
const path = require("path")

module.exports = function(api) {
  api.cache(true)

  const isProd = (process.env.NODE_ENV || "").startsWith("prod")
  const dotEnvPath = path.resolve(__dirname, isProd ? `.env` : `.env.dev`)

  if (!fs.existsSync(dotEnvPath)) {
    throw new Error(
      `Babel config couldn't find dot env file path: ${dotEnvPath}`
    )
  }

  console.info(`Loading environment variables from: ${dotEnvPath}`)

  return {
    presets: ["next/babel"],
    plugins: [
      "styled-components",
      ["inline-dotenv", { path: dotEnvPath }],
      ["module-resolver", { root: ["./src"] }]
    ]
  }
}
