import GraphqlTest from "components/GraphqlTest"
import { PageWrapper } from "components/ui/common"
import { withApollo } from "utils/apollo"

const GraphqlDemo = props => (
  <PageWrapper>
    <h2>Graphql demo data</h2>
    <GraphqlTest />
  </PageWrapper>
)

// Always use withApollo at the page level for ssr
export default withApollo(GraphqlDemo)
