import React, { Component, Fragment } from "react"
import PropTypes from "prop-types"
import { Map } from "immutable"
import { connect } from "react-redux"
import { PageWrapper } from "components/ui/common"

import { getTopRepos } from "actions/repos"
import SearchResults from "components/SearchResults"

class SearchRepoContainer extends Component {
  // // Server side redux example
  // static async getInitialProps({ store, query }) {
  //   let lang = query.lang || "python"
  //   await store.dispatch(getTopRepos({ lang }))
  // }

  componentDidMount() {
    let { dispatch } = this.props
    dispatch(getTopRepos({ lang: "javascript" }))
  }

  render() {
    let { repos } = this.props

    return (
      <PageWrapper>
        <SearchResults repos={repos} />
      </PageWrapper>
    )
  }
}

const mapState = state => ({ repos: state.repos })

SearchRepoContainer.propTypes = {
  repos: PropTypes.instanceOf(Map).isRequired
}

export { SearchRepoContainer }

export default connect(mapState)(SearchRepoContainer)
